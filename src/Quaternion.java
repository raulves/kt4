import java.util.*;

/**
 * Quaternions. Basic operations.
 */
public class Quaternion {

    private static final double EPSILON = 0.000001;

    private double a;
    private double b;
    private double c;
    private double d;

    /**
     * Constructor from four double values.
     *
     * @param a real part
     * @param b imaginary part i
     * @param c imaginary part j
     * @param d imaginary part k
     */
    public Quaternion(double a, double b, double c, double d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    /**
     * Real part of the quaternion.
     *
     * @return real part
     */
    public double getRpart() {
        return this.a;
    }

    /**
     * Imaginary part i of the quaternion.
     *
     * @return imaginary part i
     */
    public double getIpart() {
        return this.b;
    }

    /**
     * Imaginary part j of the quaternion.
     *
     * @return imaginary part j
     */
    public double getJpart() {
        return this.c;
    }

    /**
     * Imaginary part k of the quaternion.
     *
     * @return imaginary part k
     */
    public double getKpart() {
        return this.d;
    }

    /**
     * Conversion of the quaternion to the string.
     *
     * @return a string form of this quaternion:
     * "a+bi+cj+dk"
     * (without any brackets)
     */
    @Override
    public String toString() {
        return this.a + (Math.signum(this.b) < 0 ? String.valueOf(this.b) : "+" + this.b) + "i" + (Math.signum(this.c) < 0 ? String.valueOf(this.c) : "+" + this.c) + "j" + (Math.signum(this.d) < 0 ? String.valueOf(this.d) : "+" + this.d) + "k";
    }

    /**
     * Conversion from the string to the quaternion.
     * Reverse to <code>toString</code> method.
     *
     * @param s string of form produced by the <code>toString</code> method
     * @return a quaternion represented by string s
     * @throws IllegalArgumentException if string s does not represent
     *                                  a quaternion (defined by the <code>toString</code> method)
     */
    public static Quaternion valueOf(String s) {
        char[] chars = s.toCharArray();

        double realPart = 0.;
        double iPart = 0.;
        double jPart = 0.;
        double kPart = 0.;

        boolean realPartHasValue = false;
        boolean iHasValue = false;
        boolean jHasValue = false;
        boolean kHasValue = false;

        StringBuilder result = new StringBuilder();

        for (char element : chars) {

            if (element == '+' || element == '-') {

                if (result.length() > 0) {
                    if (result.charAt(result.length() - 1) == '+' || result.charAt(result.length() - 1) == '-') {
                        throw new RuntimeException("Topelt tehtemärk avaldises: " + s);
                    }
                    if (!realPartHasValue) {
                        if (iHasValue || jHasValue || kHasValue) {
                            throw new RuntimeException("realPart ei saa olla pärast teisi elemente");
                        }
                        try {
                            realPart = Double.parseDouble(result.toString());
                        }
                        catch (NumberFormatException e) {
                            throw new RuntimeException("Tegemist ei ole arvuga: " + result);
                        }
                        result = new StringBuilder();
                        realPartHasValue = true;
                    }
                    else {
                        throw new RuntimeException("realPart komponent esineb topelt avaldises: " + s);
                    }

                }

                result.append(element);
                continue;
            }

            if (Character.isLetter(element)) {
                if (element == 'i') {
                    if (!jHasValue ) {
                        if (!kHasValue) {
                            if (!iHasValue) {
                                try {
                                    iPart = Double.parseDouble(result.toString());
                                }
                                catch (NumberFormatException e) {
                                    throw new RuntimeException("Tegemist ei ole arvuga: " + result);
                                }
                                result = new StringBuilder();
                                iHasValue = true;
                                continue;
                            }
                            throw new RuntimeException("i sümbol/komponent esineb topelt avaldises: " + s);
                        }
                        else {
                            throw new RuntimeException("i ja k on vahetuses");
                        }

                    }
                    else {
                        throw new RuntimeException("i ja j on vahetuses");
                    }


                }

                if (element == 'j') {
                    if (!kHasValue) {
                        if (!jHasValue) {
                            try {
                                jPart = Double.parseDouble(result.toString());
                            }
                            catch (NumberFormatException e) {
                                throw new RuntimeException("Tegemist ei ole arvuga: " + result);
                            }
                            result = new StringBuilder();
                            jHasValue = true;
                            continue;
                        }
                        throw new RuntimeException("j sümbol/komponent esineb topelt avaldises: " + s);
                    }
                    else {
                        throw new RuntimeException("j ja k on vahetuses");
                    }

                }

                if (element == 'k') {
                    if (!kHasValue) {
                        try {
                            kPart = Double.parseDouble(result.toString());
                        }
                        catch (NumberFormatException e) {
                            throw new RuntimeException("Tegemist ei ole arvuga: " + result);
                        }
                        result = new StringBuilder();
                        kHasValue = true;
                        continue;
                    }
                    throw new RuntimeException("k sümbol/komponent esineb topelt avaldises: " + s);
                }

                throw new RuntimeException("Avaldises " + s + " on element " + element + ", mis ei kuulu Quaternion avaldisse.");
            }


            result.append(element);


        }

        if (result.length() > 0) {
            if (!realPartHasValue) {
                if (iHasValue || jHasValue || kHasValue) {
                    throw new RuntimeException("realPart ei saa olla pärast teisi elemente");
                }
                try {
                    realPart = Double.parseDouble(result.toString());
                }
                catch (NumberFormatException e) {
                    throw new RuntimeException("Tegemist ei ole arvuga: " + result);
                }
            }
            else {
                throw new RuntimeException("realPart komponent esineb topelt avaldises: " + s);
            }

        }
        return new Quaternion(realPart, iPart, jPart, kPart);
    }

    /**
     * Clone of the quaternion.
     *
     * @return independent clone of <code>this</code>
     */
    @Override
    public Object clone() throws CloneNotSupportedException {

        return new Quaternion(this.a, this.b, this.c, this.d);
    }

    /**
     * Test whether the quaternion is zero.
     *
     * @return true, if the real part and all the imaginary parts are (close to) zero
     */
    public boolean isZero() {
        return this.a < EPSILON && this.b < EPSILON && this.c < EPSILON && this.d < EPSILON
                && this.a > -EPSILON && this.b > -EPSILON && this.c > -EPSILON && this.d > -EPSILON;
    }

    /**
     * Conjugate of the quaternion. Expressed by the formula
     * conjugate(a+bi+cj+dk) = a-bi-cj-dk
     *
     * @return conjugate of <code>this</code>
     */
    public Quaternion conjugate() {
        return new Quaternion(this.a, this.b * -1, this.c * -1, this.d * -1);
    }

    /**
     * Opposite of the quaternion. Expressed by the formula
     * opposite(a+bi+cj+dk) = -a-bi-cj-dk
     *
     * @return quaternion <code>-this</code>
     */
    public Quaternion opposite() {
        return new Quaternion(this.a * -1, this.b * -1, this.c * -1, this.d * -1);
    }

    /**
     * Sum of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
     *
     * @param q addend
     * @return quaternion <code>this+q</code>
     */
    public Quaternion plus(Quaternion q) {
        return new Quaternion(this.a + q.a, this.b + q.b, this.c + q.c, this.d + q.d);
    }

    /**
     * Product of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
     * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
     *
     * @param q factor
     * @return quaternion <code>this*q</code>
     */
    public Quaternion times(Quaternion q) {
        return new Quaternion((this.a * q.a - this.b * q.b - this.c * q.c - this.d * q.d), (this.a * q.b + this.b * q.a + this.c * q.d - this.d * q.c),
                (this.a * q.c - this.b * q.d + this.c * q.a + this.d * q.b), (this.a * q.d + this.b * q.c - this.c * q.b + this.d * q.a));
    }

    /**
     * Multiplication by a coefficient.
     *
     * @param r coefficient
     * @return quaternion <code>this*r</code>
     */
    public Quaternion times(double r) {
        return new Quaternion(this.a * r, this.b * r, this.c * r, this.d * r);
    }

    /**
     * Inverse of the quaternion. Expressed by the formula
     * 1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
     * ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
     *
     * @return quaternion <code>1/this</code>
     */
    public Quaternion inverse() {
        if (new Quaternion(this.a, this.b, this.c, this.d).isZero()) {
            throw new RuntimeException("Cannot do inverse for quaternion: " + this.toString());
        }
        return new Quaternion((this.a / (this.a * this.a + this.b * this.b + this.c * this.c + this.d * this.d)),
                ((this.b * -1) / (this.a * this.a + this.b * this.b + this.c * this.c + this.d * this.d)),
                ((this.c * -1) / (this.a * this.a + this.b * this.b + this.c * this.c + this.d * this.d)),
                ((this.d * -1) / (this.a * this.a + this.b * this.b + this.c * this.c + this.d * this.d)));
    }

    /**
     * Difference of quaternions. Expressed as addition to the opposite.
     *
     * @param q subtrahend
     * @return quaternion <code>this-q</code>
     */
    public Quaternion minus(Quaternion q) {
        return new Quaternion(this.a - q.a, this.b - q.b, this.c - q.c, this.d - q.d);
    }

    /**
     * Right quotient of quaternions. Expressed as multiplication to the inverse.
     *
     * @param q (right) divisor
     * @return quaternion <code>this*inverse(q)</code>
     */
    public Quaternion divideByRight(Quaternion q) {
        if (q.isZero()) {
            throw new RuntimeException("Cannot do divideByRight for quaternion: " + this.toString());
        }
        return this.times(q.inverse());
    }

    /**
     * Left quotient of quaternions.
     *
     * @param q (left) divisor
     * @return quaternion <code>inverse(q)*this</code>
     */
    public Quaternion divideByLeft(Quaternion q) {
        if (q.isZero()) {
            throw new RuntimeException("Cannot do divideByLeft for quaternion: " + this.toString());
        }
        return q.inverse().times(this);
    }

    /**
     * Equality test of quaternions. Difference of equal numbers
     * is (close to) zero.
     *
     * @param qo second quaternion
     * @return logical value of the expression <code>this.equals(qo)</code>
     */
    @Override
    public boolean equals(Object qo) {
        if ((((Quaternion) qo).a - this.a) < EPSILON && (((Quaternion) qo).b - this.b) < EPSILON && (((Quaternion) qo).c - this.c) < EPSILON && (((Quaternion) qo).d - this.d) < EPSILON
                && (((Quaternion) qo).a - this.a) > -EPSILON && (((Quaternion) qo).b - this.b) > -EPSILON && (((Quaternion) qo).c - this.c) > -EPSILON && (((Quaternion) qo).d - this.d) > -EPSILON) {
            return true;
        }
        return false;
    }

    /**
     * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
     *
     * @param q factor
     * @return dot product of this and q
     */
    public Quaternion dotMult(Quaternion q) {
        return (this.times(q.conjugate()).plus(q.times(this.conjugate()))).times(0.5);
    }

    /**
     * Integer hashCode has to be the same for equal objects.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }

    /**
     * Norm of the quaternion. Expressed by the formula
     * norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
     *
     * @return norm of <code>this</code> (norm is a real number)
     */
    public double norm() {
        return Math.sqrt(this.a * this.a + this.b * this.b + this.c * this.c + this.d * this.d);
    }

    /**
     * Main method for testing purposes.
     *
     * @param arg command line parameters
     */
    public static void main(String[] arg) {
        System.out.println(Quaternion.valueOf("-1+-8i"));
      /*
      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: " 
         + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));

       */
    }
}
// end of file
